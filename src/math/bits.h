#ifndef _SSD_MATH_BITS_H
#define _SSD_MATH_BITS_H

#include <algorithm>
#include <vector>

namespace ssd {

//! @brief count the number of bits in n
inline int bit_count(int n) {
  int count = 1;
  while (std::pow(2, count) <= n)
    count++;
  return count;
}
constexpr int bit_count_const(int n) { return bit_count(n); }

//! @brief return the value of the kth bit in n
inline bool kbit(int n, int k) { return (n & (1 << k)) >> k; }
constexpr bool kbit_const(int n, int k) { return kbit(n,k); }

//! @brief convert int n into vector of bool
inline auto itobv(int n, int k = 0) -> std::vector<bool> {
  int b = bit_count(n);
  std::vector<bool> v;

  // if bitsize is smaller than n int
  if (b > k)
    throw smt::runtime_error{"invalid bit size in itobv"};

  // convert int to vector of bool
  for (int i = 0; i <= b - 1; i++)
    v.push_back(kbit(n, i));

  // fill in zeros for extra bits
  for (int i = 0; i < k - b; i++)
    v.push_back(0);
  std::reverse(std::begin(v), std::end(v));

  return v;
}

} // namespace ssd

#endif
