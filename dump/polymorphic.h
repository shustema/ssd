#ifndef POLYMORPHIC_H
#define POLYMORPHIC_H

#include <type_traits>

//! @brief macro for enable_if on type condition
#define T_SAME(...) MACRO_DISPATCH(T_SAME, __VA_ARGS__)(__VA_ARGS__)
#define T_SAME2(T, U) typename std::enable_if<std::is_same<T, U>::value, void>::type
#define T_SAME3(T, U, W) typename std::enable_if<std::is_same<T, U>::value, W>::type

namespace smt {
template <typename T, typename _ = void> struct is_container : std::false_type {};
namespace internal {
template <typename... Ts> struct is_container_helper {};
} // namespace internal
template <typename T>
struct is_container<
    T, std::conditional_t<
           false,
           internal::is_container_helper<
               typename T::value_type, typename T::size_type, typename T::allocator_type,
               typename T::iterator, typename T::const_iterator,
               decltype(std::declval<T>().size()), decltype(std::declval<T>().begin()),
               decltype(std::declval<T>().end()), decltype(std::declval<T>().cbegin()),
               decltype(std::declval<T>().cend())>,
           void>> : public std::true_type {};

//! @brief SFINEA function to enable on any of the types
//!
//! Use this with static_assert( is_any_of<T, ...>{}, "...");
//! Or typename enable_if<is_any_of<T, ...>, U>
//! @{
template <typename T, typename U, typename... Us>
struct is_any_of
    : std::integral_constant<bool, std::conditional<std::is_same<T, U>::value, std::true_type,
                                                    is_any_of<T, Us...>>::type::value> {};
template <typename T, typename U> struct is_any_of<T, U> : std::is_same<T, U>::type {};
//! @}

} // namespace smt

#endif
