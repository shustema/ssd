#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <fstream>
#include <iostream>

#include "main/exception.h"

namespace smt::filesystem {
inline void cp(std::string s, std::string d) {
  std::cout << "Copying " << s << " to " << d << std::endl;
  std::string line;
  std::ifstream ini_file{s};
  std::ofstream out_file{d};

  if (ini_file && out_file) {
    while (getline(ini_file, line))
      out_file << line << '\n';
    std::cout << "Coppied " << s << " to " << d << std::endl;
  } else {
    std::cout << "Failed to copy " << s << " to " << d << std::endl;
    ini_file.close();
    out_file.close();
    throw smt::runtime_error{"Cannot read file"};
  }
  ini_file.close();
  out_file.close();
}
} // namespace smt::filesystem

#endif
