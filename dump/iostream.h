#ifndef IOSTREAM_H
#define IOSTREAM_H

#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>

namespace smt {

//! @brief remove spaces in a string
inline void compact(std::string &s) {
  s.erase(std::remove_if(s.begin(), s.end(), ::isspace), s.end());
}

//! @brief convert string to uppercase
inline void toupper(std::string &s) {
  std::transform(s.begin(), s.end(), s.begin(), ::toupper);
}

//! @brief get vectorstring for line by delimiters
inline std::vector<std::string> split_bench(std::string s, std::string delimiters) {
  std::vector<std::string> ret;
  boost::split(ret, s, boost::is_any_of(delimiters));
  for (auto i = ret.begin(); i != ret.end();) {
    if (std::count_if(i->begin(), i->end(),
                      [](unsigned char c) { return !std::isspace(c); })) {
      compact(*i);
      toupper(*i);
      i++;
    } else
      ret.erase(i);
  }
  return ret;
}

inline std::string shorten(std::string s) {
  std::string ret;
  unique_copy(s.begin(), s.end(), std::back_insert_iterator<std::string>(ret),
              [](char a, char b) { return std::isspace(a) && isspace(b); });
  return ret;
}

inline std::vector<std::string> split_line(std::string s, std::string delimiters) {
  std::vector<std::string> ret;
  boost::split(ret, s, boost::is_any_of(delimiters));
  for (auto i = ret.begin(); i != ret.end();)
    if (std::count_if(i->begin(), i->end(), [](unsigned char c) { return !std::isspace(c); }))
      i++;
    else
      ret.erase(i);
  return ret;
}

inline bool is_content(std::string bench_file_s, bool verilog = false) {
  if (bench_file_s.length() == 0)
    return false;

  // bypass empty lines
  if (bench_file_s[0] == '\r')
    return false;

  if (verilog) {
    if (bench_file_s[0] == '/' && bench_file_s[1] == '/')
      return false;
  } else {
    // bypass comments
    if (bench_file_s[0] == '#')
      return false;
  }

  return true;
}

//! @brief split a string by delimiters
inline std::vector<std::string> spltstr(std::string s, std::string delimiters) {
  std::vector<std::string> ret;
  boost::split(ret, s, boost::is_any_of(delimiters));
  for (auto i = ret.begin(); i != ret.end();)
    if (std::count_if(i->begin(), i->end(), [](unsigned char c) { return !std::isspace(c); }))
      i++;
    else
      ret.erase(i);
  return ret;
}

//! @brief split a string by a key
inline std::pair<std::string, std::string> parsestr(std::string s, std::string delimiter) {
  size_t pos = 0;
  std::string first;
  while ((pos = s.find(delimiter)) != std::string::npos) {
    first = s.substr(0, pos);
    s.erase(0, pos + delimiter.length());
  }
  if (first == "")
    return {s, first};
  return {first, s};
}

//! @brief reset stringstream buffer
inline void reset(std::stringstream &s) {
  const static std::stringstream initial;
  s.str(std::string());
  s.clear();
  s.copyfmt(initial);
}

//! @brief concatenate printable objects into a string
template <typename... Params> std::string concat(Params const &... params) {
  std::stringstream ss;
  ((ss << params), ...);
  return ss.str();
}

//! @brief cout/cin buffer redirection with destructors
//! @{
namespace internal {
//! @brief redirect cout
struct _coutbuf {
  std::streambuf *coutbuf = std::cout.rdbuf();
  std::fstream _file;
  inline _coutbuf() {}
  inline _coutbuf(const char *coutf) {
    _file.open(coutf, std::iostream::out);
    std::streambuf *stream_buffer_file = _file.rdbuf();
    std::cout.rdbuf(stream_buffer_file);
  }
  inline _coutbuf(std::string coutf) {
    _file.open(coutf, std::iostream::out);
    std::streambuf *stream_buffer_file = _file.rdbuf();
    std::cout.rdbuf(stream_buffer_file);
  }
  inline ~_coutbuf() {
    std::cout.rdbuf(coutbuf);
    _file.close();
  }
};

//! @brief redirect cin
struct _cinbuf {
  std::streambuf *cinbuf = std::cin.rdbuf();
  std::fstream _file;
  inline _cinbuf() {}
  inline _cinbuf(const char *cinf) {
    _file.open(cinf, std::iostream::out);
    std::streambuf *stream_buffer_file = _file.rdbuf();
    std::cin.rdbuf(stream_buffer_file);
  }
  inline _cinbuf(std::string cinf) {
    _file.open(cinf, std::iostream::out);
    std::streambuf *stream_buffer_file = _file.rdbuf();
    std::cin.rdbuf(stream_buffer_file);
  }
  inline ~_cinbuf() {
    std::cin.rdbuf(cinbuf);
    _file.close();
  }
};

} // namespace internal
//! @}

inline std::string run_script(std::string command) {
  std::string ret, file;
  FILE *pipe{popen(command.c_str(), "r")};
  char buffer[256];
  while (fgets(buffer, sizeof(buffer), pipe) != nullptr) {
    file = buffer;
    ret += file.substr(0, file.size() - 1);
  }
  pclose(pipe);
  return ret;
}

//! @brief macros to call redirection
//! @{
#define COUT(FILE) smt::internal::_coutbuf coutbuf(FILE)
#define CIN(FILE) smt::internal::_cinbuf cinbuf(FILE)
//! @}

inline int ctoi(char c) { return (int)c - 48; }

} // namespace smt

#endif
