#ifndef MACRO_H
#define MACRO_H

//! @brief count input arguments
#define VA_NUM_ARGS(...) VA_NUM_ARGS_(__VA_ARGS__, 5, 4, 3, 2, 1)
#define VA_NUM_ARGS_(_1, _2, _3, _4, _5, N, ...) N

//! @brief dispatch macro for variadic macros
#define MACRO_DISPATCH(func, ...) MACRO_DISPATCH_(func, VA_NUM_ARGS(__VA_ARGS__))
#define MACRO_DISPATCH_(func, nargs) MACRO_DISPATCH__(func, nargs)
#define MACRO_DISPATCH__(func, nargs) func##nargs

#endif
