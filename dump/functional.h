#ifndef FUNCTIONAL_H
#define FUNCTIONAL_H

#include <boost/iterator/zip_iterator.hpp>
#include <boost/range.hpp>

namespace smt {

//! @brief check if something is any of a list of inputs
//! @{
template <typename T, int TSize> struct any_of_ {
  template <typename TFirst, typename... TOthers>
  explicit any_of_(TFirst &&first, TOthers &&... others)
      : values({std::forward<TFirst>(first), std::forward<TOthers>(others)...}) {}

  std::array<T, TSize> values;
};

template <typename TFirst, typename... TOthers>
auto any_of(TFirst &&first, TOthers &&... others) {
  constexpr std::size_t size = 1 + sizeof...(others);
  return any_of_<typename std::decay<TFirst>::type, size>(std::forward<TFirst>(first),
                                                          std::forward<TOthers>(others)...);
}

template <typename T, int TSize>
bool operator==(const T value, const any_of_<typename std::decay<T>::type, TSize> &any_of_) {
  return std::find(any_of_.values.begin(), any_of_.values.end(), value) !=
         any_of_.values.end();
}
template <typename T, int TSize>
bool operator!=(const T value, const any_of_<typename std::decay<T>::type, TSize> &any_of_) {
  return !(value == any_of_);
}
//! @}

//! @brief split a vector in place, and return the split portion
template <typename T> std::vector<T> slice_t(std::vector<T> &v, int m, int n) {
  std::vector<T> ret, vmod;
  std::copy(v.begin() + m, v.begin() + n + 1, std::back_inserter(ret));
  std::copy(v.begin() + n + 1, v.begin() + v.size(), std::back_inserter(vmod));
  v = vmod;
  return ret;
}

//! @brief split a vector in place, and return the split portion
template <typename T> std::vector<T> slice(std::vector<T> &v, int m, int n) {
  std::vector<T> ret;
  std::copy(v.begin() + m, v.begin() + n + 1, std::back_inserter(ret));
  return ret;
}

//! @brief container functions
//! @{
namespace internal {

template <typename T> struct has_const_iterator {
private:
  template <typename C> static char test(typename C::const_iterator *);
  template <typename C> static int test(...);

public:
  enum { value = sizeof(test<T>(0)) == sizeof(char) };
};

} // namespace internal

//! @brief check if a container has a given value
//! @details only enabled if container value type is the same as the input value
//! type
template <typename T>
auto contains(const T &c, typename T::value_type const &i) ->
    typename std::enable_if<internal::has_const_iterator<T>::value, bool>::type {
  if (std::find(c.begin(), c.end(), i) != c.end())
    return true;
  return false;
}

//! @brief find and remove an element from a vector
template <typename T>
auto remove(const T &c, typename T::value_type const &i) ->
    typename std::enable_if<internal::has_const_iterator<T>::value, void>::type {
  c.erase(std::find(c.begin(), c.end(), i));
}
//! @}

//! @brief merge two containers of the same type
//! @{
namespace internal {
template <typename...> struct and_;
template <> struct and_<> : public std::true_type {};
template <typename B1> struct and_<B1> : public B1 {};
template <typename B1, typename B2>
struct and_<B1, B2> : public std::conditional<B1::value, B2, B1>::type {};
template <typename B1, typename B2, typename B3, typename... Bn>
struct and_<B1, B2, B3, Bn...>
    : public std::conditional<B1::value, and_<B2, B3, Bn...>, B1>::type {};
} // namespace internal

//! @brief merge two containers of the same type
template <typename T, typename... Ts>
auto merge(const T arg, const Ts... args) ->
    typename std::enable_if<internal::and_<std::is_same<T, Ts>...>::value, T>::type {
  T ret;
  ret.insert(ret.end(), arg.begin(), arg.end());
  (ret.insert(ret.end(), args.begin(), args.end()), ...);
  return ret;
}
//! @}

template <typename T> auto subvector(std::vector<T> const &v, int m, int n) -> std::vector<T> {
  auto first = v.begin() + m;
  auto last = v.begin() + n + 1;
  std::vector<T> ret(first, last);
  return ret;
}
template <typename T> auto subvector(std::vector<T> const &v, int m) -> std::vector<T> {
  auto first = v.begin() + m;
  auto last = v.end();
  std::vector<T> ret(first, last);
  return ret;
}

} // namespace smt

#endif
