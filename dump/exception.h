#ifndef EXCEPTION_H
#define EXCEPTION_H 1

#include <cerrno>
#include <cstring>
#include <exception>
#include <iostream>
#include <utility>

#include <bits/c++config.h>

#pragma GCC visibility push(default)

#if defined __GNUC__
#ifndef _SMTCXX_NOTHROW
#if __cplusplus >= 201403L
#define _SMTCXX_NOTHROW noexcept
#elif __cplusplus < 201403L
#define _SMTCXX_NOTHROW throw()
#else // __cplusplus
#define _SMTCXX_NOTHROW __attribute__((__nothrow__))
#endif //  __cplusplus
#endif // _SMTCXX_NOTHROW
#else  // __GNUC__
#define _SMTCXX_NOTHROW
#endif // _GNUC__

namespace smt _GLIBCXX_VISIBILITY(default) {
  _GLIBCXX_BEGIN_NAMESPACE_VERSION
  /**
   * @defgroup exceptions Exceptions
   * @ingroup diagnostics
   *
   * Classes and functions for reporting errors via exception classes.
   * @{
   */

  /**
   * @brief Base class for all library exceptions.
   *
   * This is the base class for all exceptions thrown by the smt library.
   */
  struct exception : virtual public std::exception {
    using std::exception::exception;
    explicit exception(const char *msg, int err = 3) _SMTCXX_NOTHROW : msg_(msg), err_(err) {}
    explicit exception(std::string &msg, int err = 3) _SMTCXX_NOTHROW : msg_(msg), err_(err) {}

    explicit exception(std::string msg, int err = 3) _SMTCXX_NOTHROW : msg_(msg), err_(err) {}
    virtual ~exception() _SMTCXX_NOTHROW {}
    const char *what() const _SMTCXX_NOTHROW override { return msg_.c_str(); }
    const int how() const _SMTCXX_NOTHROW { return err_; }
    const char *info() const _SMTCXX_NOTHROW { return std::strerror(err_); }

  protected:
    std::string msg_;
    int err_ = -1;
  };

  /**
   * @brief Exception derived class for runtime detectable errors
   *
   * This is the base class for all runtime only errors that are due to an external
   * problem. These errors also encompas all unrecoverable errors
   */
  struct runtime_error : virtual public smt::exception {
    using smt::exception::exception;
    runtime_error(const runtime_error &o) _SMTCXX_NOTHROW { msg_ = o.msg_, err_ = o.err_; }
    runtime_error(runtime_error &&o) _SMTCXX_NOTHROW {
      msg_ = std::move(o.msg_), err_ = std::exchange(o.err_, -1);
    }
  };

  /**
   * @brief Exception derived class for internal logic errors
   *
   * This is the base class for all internal logic errors. This exception type will
   * usually be non-blocking
   */
  struct logic_error : virtual public smt::exception {
    using smt::exception::exception;
    logic_error(const logic_error &o) _SMTCXX_NOTHROW { msg_ = o.msg_, err_ = o.err_; }
    logic_error(logic_error &&o) _SMTCXX_NOTHROW {
      msg_ = std::move(o.msg_), err_ = std::exchange(o.err_, -1);
    }
  };

  /**
   * @brief Function for printing nested exceptions
   * @{
   */
  inline void handle_exception(const smt::exception &e, int i = 1) {
    if (typeid(e) == typeid(smt::runtime_error))
      std::cout << "Runtime Error: " << e.what() << std::endl;
    else if (typeid(e) == typeid(smt::logic_error))
      std::cout << "Internal Error: " << e.what() << std::endl;
    else
      std::cout << "Error: " << e.what() << std::endl;
    std::cout << std::string(i, '\t') << "[" << e.how() << "]\n";
    std::cout << std::string(i, '\t') << e.info() << std::endl;
    try {
      std::rethrow_if_nested(e);
    } catch (const exception &nested) {
      handle_exception(nested, i + 1);
    }
  }
  inline void handle_exception(const std::exception &e, int i = 1) {
    if (typeid(e) == typeid(std::exception))
      std::cout << "Internal Error: " << e.what() << std::endl;
    else
      std::cout << "Error: " << e.what() << std::endl;
    // std::cout << std::string(i, '\t') << "[" << e.how() << "]\n";
    // std::cout << std::string(i, '\t') << e.info() << std::endl;
    try {
      std::rethrow_if_nested(e);
    } catch (const exception &nested) {
      handle_exception(nested, i + 1);
    }
  }
  //! @}

  _GLIBCXX_END_NAMESPACE_VERSION
} // namespace smt_GLIBCXX_VISIBILITY(default)

#pragma GCC visibility pop

#endif
