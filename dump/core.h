#ifndef CORE_H
#define CORE_H

#include <chrono>
#include <iomanip>
#include <iostream>
#include <limits.h>
#include <string>

#include "PP.h"

#define INF INT_MAX
#define BIT(x) (x << 1)
#define PASS (void)0

/**
 * @file
 * @brief core function definitions and smt namespace initialization for project
 * wide utilities
 */

/**
 * @namespace
 * @brief namespace for split manufacturing tool
 */
namespace smt {

//! @brief tuple parameter pack parsing
template <int N, typename... Ts>
using NthTypeOf = typename std::tuple_element<N, std::tuple<Ts...>>::type;

/**
 * @brief class for measuring execution time of a scope
 * called using EXECUTION_TIME macro which pulls the execution time of the
 * current
 * __FUNCTION__ internal g++ parameter, and displays it to std::cout
 */
class ExecutionTime {
private:
  const std::chrono::steady_clock::time_point begin;
  const std::string caller;

public:
  ExecutionTime(const std::string &caller)
      : caller(caller), begin(std::chrono::steady_clock::now()) {
    std::cout << std::setw(50) << std::setfill('-') << std::right << '\n';
    std::cout << std::setfill(' ');
    std::cout << "Running _function " << caller << "\n";
    std::cout << "\tInstantiating timer"
              << "\n\n ";
    std::cout << std::flush;
  }

  ~ExecutionTime() {
    const auto duration = std::chrono::steady_clock::now() - begin;
    std::cout << "Execution Time:";
    std::cout << std::setw(21) << std::setfill(' ') << std::internal;
    std::cout << "[" << caller << "]";
    std::cout << std::setfill('*') << std::setw(10) << std::right;
    std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(duration).count()
              << "ns\n";
    std::cout << std::setw(50) << std::setfill('-') << std::right << '\n';
    std::cout << std::setfill(' ');
    std::cout << std::flush;
  }
};
} // namespace smt

//! @brief macro call for execution time class
#ifdef SILENT
#define EXECUTION_TIME
#else
#define EXECUTION_TIME const smt::ExecutionTime execution_time(__FUNCTION__);
#endif

#endif /* CORE_H */
