#include <iostream>

#include "crtpl.h"

template <typename Derived>
class test_fixture : public ssd::crtpl<Derived> {
  public:
    CRTPL_FUNC(print) { std::cout << "base class" << std::endl; }
    //CRTPL_FUNC_CONST_T(FORMAT_RETURN_TYPE([[nodiscard]] int), doNothing, FORMAT_ARGUMENTS(int a, int b, int c));
};

class derived : public test_fixture<derived> {
  public:
  void crtpl_print() { std::cout << "is this working?" << std::endl; }
};
class derived2 : public test_fixture<derived2> {};

auto main() -> int {

  derived d;
  d.print();

  derived2 d2;
  d2.print();

  return 0;
}
