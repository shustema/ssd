#ifndef _SSD_CRTPL_H
#define _SSD_CRTPL_H

#define GLUE(x, y) x y

#define RETURN_ARG_COUNT(_1_, _2_, _3_, _4_, _5_, count, ...) count
#define EXPAND_ARGS(args) RETURN_ARG_COUNT args
#define COUNT_ARGS_MAX5(...) EXPAND_ARGS((__VA_ARGS__, 5, 4, 3, 2, 1, 0))

#define OVERLOAD_MACRO2(name, count) name##count
#define OVERLOAD_MACRO1(name, count) OVERLOAD_MACRO2(name, count)
#define OVERLOAD_MACRO(name, count) OVERLOAD_MACRO1(name, count)

#define CALL_OVERLOAD(name, ...) GLUE(OVERLOAD_MACRO(name, COUNT_ARGS_MAX5(__VA_ARGS__)), (__VA_ARGS__))

#ifndef FORMAT_RETURN_TYPE
#define FORMAT_RETURN_TYPE(...) __VA_ARGS__
#endif 

#ifndef FORMAT_ARGUMENTS
#define FORMAT_ARGUMENTS(...) __VA_ARGS__
#endif

#ifndef CRTPL_FUNC
#define CRTPL_FUNC3(FUNC_NAME, RET_TYPE, ARGS) \
  RET_TYPE FUNC_NAME(ARGS) { return this->underlying()->crtpl_##FUNC_NAME(ARGS); } \
  RET_TYPE crtpl_##FUNC_NAME(ARGS)
#define CRTPL_FUNC2(FUNC_NAME, RET_TYPE) \
  RET_TYPE FUNC_NAME() { return this->underlying()->crtpl_##FUNC_NAME(); } \
  RET_TYPE crtpl_##FUNC_NAME()
#define CRTPL_FUNC1(FUNC_NAME) \
  void FUNC_NAME() { this->underlying()->crtpl_##FUNC_NAME(); } \
  void crtpl_##FUNC_NAME()
#define CRTPL_FUNC(...) CALL_OVERLOAD(CRTPL_FUNC,  __VA_ARGS__)
#endif

#ifndef CRTPL_FUNC_CONST_T
#define CRTPL_FUNC_CONST_T(RET_TYPE, FUNC_NAME, ARGS) \
  RET_TYPE FUNC_NAME(ARGS) const { return this->underlying()->crtpl_##FUNC_NAME(ARGS); } \
  RET_TYPE crtpl_##FUNC_NAME(ARGS) const 
#endif
#ifndef CRTPL_FUNC_CONST
#define CRTPL_FUNC_CONST(RET_TYPE, FUNC_NAME, ARGS) \
  void FUNC_NAME(ARGS) const { this->underlying()->crtpl_##FUNC_NAME(ARGS); } \
  void crtpl_##FUNC_NAME(ARGS) const 
#endif

namespace ssd {
  template <typename T>
    struct crtpl {
      [[nodiscard]] auto      underlying() -> T&             { return static_cast<T&>(*this); }
      [[nodiscard]] auto      underlying() const -> T const& { return static_cast<T const&>(*this); }
    };

  //template <typename T, template<typename> class crtplType>
  //  struct crtpl {
  //    [[nodiscard]] auto      underlying() -> T&             { return static_cast<T&>(*this); }
  //    [[nodiscard]] auto      underlying() const -> T const& { return static_cast<T const&>(*this); }
  //    private:
  //    crtpl(){}
  //    friend crtplType<T>;
  //  };
}

#endif // _SSD_CRTPL_H
