#ifndef _SSD_PREPROCESSOR_SWITCH_H
#define _SSD_PREPROCESSOR_SWITCH_H

#define SWITCH(X)                                                                             \
  __switch_continue__ = 0;                                                                    \
  for (char *__switch_p__ = X, __switch_next__ = 1; __switch_p__ != 0; __switch_next__ = 2) { \
    if (__switch_next__ == 2) {                                                               \
      __switch_continue__ = 1;                                                                \
      break;

//!@brief instantiate case
#define CASE(X)                                                                               \
  }                                                                                           \
  if (!__switch_next__ || !(__switch_next__ = strcmp(__switch_p__, X))) {

//!@brief instantiate default case (required)
#define DEFAULT(X)                                                                            \
  }                                                                                           \
  {

//!@brief end switch case statement
#define END                                                                                   \
  __switch_p__ = 0;                                                                           \
  }                                                                                           \
  }

#define CONTINUE                                                                              \
  __switch_p__ = 0;                                                                           \
  }                                                                                           \
  }                                                                                           \
  if (__switch_continue__) {                                                                  \
    continue;                                                                                 \
  }

#endif
