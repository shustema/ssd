#ifndef _SSD_TIMECAST_H
#define _SSD_TIMECAST_H

namespace ssd {

template <class... Durations, class DurationIn>
std::tuple<Durations...> break_down_durations(DurationIn d) {
  std::tuple<Durations...> retval;
  using discard = int[];
  (void)discard{
      0,
      (void(((std::get<Durations>(retval) = std::chrono::duration_cast<Durations>(d)),
             (d -= std::chrono::duration_cast<DurationIn>(std::get<Durations>(retval))))),
       0)...};
  return retval;
}

inline std::chrono::microseconds up(std::chrono::nanoseconds time) {
  return std::chrono::round<std::chrono::microseconds>(time);
}
inline std::chrono::milliseconds up(std::chrono::microseconds time) {
  return std::chrono::round<std::chrono::milliseconds>(time);
}
inline std::chrono::seconds up(std::chrono::milliseconds time) {
  return std::chrono::round<std::chrono::seconds>(time);
}
inline std::chrono::minutes up(std::chrono::seconds time) {
  return std::chrono::round<std::chrono::minutes>(time);
}
inline std::chrono::hours up(std::chrono::minutes time) {
  return std::chrono::round<std::chrono::hours>(time);
}

#endif //_SSD_TIMECAST_H
