#include <iostream> 

#include "execute.h"

void foo() {
  EXECUTION_TIME;
  std::cout << "doing something random" << std::endl;
}

void bar(const std::ostream& os) {
  EXECUTION_TIME_SET_OUTPUT_STREAM(os);
  std::cout << "doing something random" << std::endl;
}

int main() {
  foo();
  bar(std::cout);

  return 0;
}
