#ifndef _SSD_CHRONO_EXECUTE_H
#define _SSD_CHRONO_EXECUTE_H

#include <iostream>
#include <iomanip>
#include <chrono>

namespace ssd {
namespace _chrono {
struct _execution_time {
            _execution_time(const std::string&);
            ~_execution_time();
  constexpr static void setStringStream(const std::ostream& os) { os_ = os; }
  [[unused]] const std::string& formatOutput(std::string);
private:
  const std::chrono::steady_clock::time_point   begin_;
  const std::string&                            caller_;
  constexpr static std::ostream&                os_ = std::cout;
};

_execution_time::_execution_time(const std::string& caller) : 
  caller_(caller), 
  begin_(std::chrono::steady_clock::now()) {
  os_ << std::setw(50) << std::setfill('-') << std::right << '\n'
      << std::setfill(' ')
      << "Running _function " << caller_ << "\n"
      << "\tInstantiating timer" << "\n\n "
      << std::flush;
}

_execution_time::~_execution_time() {
  os_ << std::setw(50) << std::setfill('-') << std::right << '\n'
      << std::setfill(' ')
      << "Running _function " << caller_ << "\n"
      << "\tInstantiating timer" << "\n\n "
      << std::flush;
}
} //  namespace _chrono

#ifndef EXECUTION_TIME
#define EXECUTION_TIME \
  ::ssd::_chrono::_execution_time _execution_time_init(__FUNCTION__)
#else
#error EXECUTION_TIME has already been defined somewhere 
#endif 

#ifndef EXECUTION_TIME_SET_OUTPUT_STREAM
#define EXECUTION_TIME_SET_OUTPUT_STREAM(OSTREAM) \
  ::ssd::_chrono::_execution_time::setStringStream(OSTREAM)
#else
#error EXECUTION_TIME_T has already been defined somewhere 
#endif

}

#endif // _SSD_EXECUTE_H
