#pragma once

#include <iostream>

/**
 * @file
 * @brief api for debugging and loging
 */

#ifdef _DEBUG
#define MESSAGE(x) (std::cout << __FILE__ << " " << __LINE__ << " " << x);
#else
#define MESSAGE(x) ;
#endif
